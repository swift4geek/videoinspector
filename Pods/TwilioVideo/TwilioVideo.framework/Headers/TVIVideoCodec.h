//
//  TVIVideoCodec.h
//  TwilioVideo
//
//  Copyright © 2017 Twilio, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NSString *const TVIVideoCodec NS_STRING_ENUM;

/**
 * @brief Block-oriented motion-compensation-based video compression standard.
 *
 * @discussion The H.264 codec uses the Constrained Baseline Profile @ Level 3.1. The H.264 codec is hardware 
 * accelerated on all iOS devices, whereas the VP8 & VP9 codecs are software implementations.
 *
 * @see [H.264](https://en.wikipedia.org/wiki/H.264/MPEG-4_AVC)
 */
FOUNDATION_EXPORT _Nonnull TVIVideoCodec TVIVideoCodecH264;

/**
 * @brief Traditional block-based transform coding format similar to H264.
 *
 * @see [VP8](https://en.wikipedia.org/wiki/VP8)
 */
FOUNDATION_EXPORT _Nonnull TVIVideoCodec TVIVideoCodecVP8;

/**
 * @brief Traditional block-based transform coding format similar to MPEG's High Efficiency Video
 * Coding (HEVC/H.265).
 *
 * @see [VP9](https://en.wikipedia.org/wiki/VP9)
 */
FOUNDATION_EXPORT _Nonnull TVIVideoCodec TVIVideoCodecVP9;
