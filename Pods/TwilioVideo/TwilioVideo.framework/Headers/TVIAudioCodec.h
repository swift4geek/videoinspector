//
//  TVIAudioCodec.h
//  TwilioVideo
//
//  Copyright © 2017 Twilio, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NSString *const TVIAudioCodec NS_STRING_ENUM;

/**
 * @brief Internet speech audio codec.
 *
 * @see [iSAC](https://en.wikipedia.org/wiki/Internet_Speech_Audio_Codec)
 */
FOUNDATION_EXPORT _Nonnull TVIAudioCodec TVIAudioCodecISAC;

/**
 * @brief Lossy audio coding format.
 *
 * @see [Opus](https://en.wikipedia.org/wiki/Opus_(audio_format))
 */
FOUNDATION_EXPORT _Nonnull TVIAudioCodec TVIAudioCodecOpus;

/**
 * @brief ITU-T standard for audio companding.
 *
 * @see [PCMA](https://en.wikipedia.org/wiki/G.711)
 */
FOUNDATION_EXPORT _Nonnull TVIAudioCodec TVIAudioCodecPCMA;

/**
 * @brief ITU-T standard for audio companding.
 *
 * @see [PCMU](https://en.wikipedia.org/wiki/G.711)
 */
FOUNDATION_EXPORT _Nonnull TVIAudioCodec TVIAudioCodecPCMU;

/**
 * @brief ITU-T standard 7 kHz Wideband audio codec.
 *
 * @see [G.722](https://en.wikipedia.org/wiki/G.722)
 */
FOUNDATION_EXPORT _Nonnull TVIAudioCodec TVIAudioCodecG722;
