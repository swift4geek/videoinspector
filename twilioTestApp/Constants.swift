//
//  Constants.swift
//  twilioTestApp
//
//  Created by Vladimir Valter on 19/01/2018.
//  Copyright © 2018 Vladimir Valter. All rights reserved.
//

import Foundation

typealias CompletionHandler = (_ Success: Bool) -> ()

let BASE_URL = "http://video.dev.rgs.digital:5000/twilio/token/?identity=valt"

