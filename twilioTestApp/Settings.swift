//
//  Settings.swift
//  twilioTestApp
//
//  Created by Vladimir Valter on 18/01/2018.
//  Copyright © 2018 Vladimir Valter. All rights reserved.
//

import TwilioVideo

class Settings: NSObject {

    let supportedAudioCodecs = [TVIAudioCodec.ISAC,
                                TVIAudioCodec.opus,
                                TVIAudioCodec.PCMA,
                                TVIAudioCodec.PCMU,
                                TVIAudioCodec.G722]

    let supportedVideoCodecs = [TVIVideoCodec.VP8,
                                TVIVideoCodec.H264,
                                TVIVideoCodec.VP9]

    var audioCodec: TVIAudioCodec?
    var videoCodec: TVIVideoCodec?

    var maxAudioBitrate = UInt()
    var maxVideoBitrate = UInt()

    func getEncodingParameters() -> TVIEncodingParameters?  {
        if maxAudioBitrate == 0 && maxVideoBitrate == 0 {
            return nil;
        } else {
            return TVIEncodingParameters(audioBitrate: maxAudioBitrate,
                                         videoBitrate: maxVideoBitrate)
        }
    }

    private override init() {
     
    }

    // MARK: Shared Instance
    static let shared = Settings()
}
