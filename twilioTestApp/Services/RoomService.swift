//
//  RoomService.swift
//  twilioTestApp
//
//  Created by Vladimir Valter on 19/01/2018.
//  Copyright © 2018 Vladimir Valter. All rights reserved.
//

import Foundation
import Alamofire

class RoomService {

    static let instance = RoomService()

    var room = Room()

    func fetchRoom(completion: @escaping CompletionHandler) {
        Alamofire.request(BASE_URL).responseJSON { (response) in

            if response.result.error == nil {
                guard let data = response.data else {return}

                do {
                    self.room = try JSONDecoder().decode(Room.self, from: data)
                } catch let err {
                    debugPrint(err as Any)
                }
                print(self.room)
                completion(true)

            } else {
                completion(false)
                debugPrint(response.result.error as Any)
            }
        }
    }
}
