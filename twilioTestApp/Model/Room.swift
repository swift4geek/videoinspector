//
//  Room.swift
//  twilioTestApp
//
//  Created by Vladimir Valter on 19/01/2018.
//  Copyright © 2018 Vladimir Valter. All rights reserved.
//

import Foundation

struct Room: Decodable {
    public private(set) var token: String!
    public private(set) var room: String!
}
